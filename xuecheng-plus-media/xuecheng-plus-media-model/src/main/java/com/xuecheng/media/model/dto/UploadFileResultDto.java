package com.xuecheng.media.model.dto;

import com.xuecheng.media.model.po.MediaFiles;
import lombok.Data;
import lombok.ToString;

/**
 * @author VectorX
 * @version 1.0.0
 * @description 上传普通文件成功响应结果
 * @date 2024/04/11
 * @see MediaFiles
 */
@Data
@ToString
public class UploadFileResultDto extends MediaFiles
{

}
