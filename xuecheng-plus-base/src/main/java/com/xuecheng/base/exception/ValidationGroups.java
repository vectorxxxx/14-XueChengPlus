package com.xuecheng.base.exception;

/**
 * @author VectorX
 * @version V1.0
 * @description 校验分组
 * @date 2024-04-09 10:10:47
 */
public class ValidationGroups
{
    public interface Insert {}

    public interface Update {}

    public interface Delete {}
}
